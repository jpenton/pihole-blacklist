const fs = require('fs');

let domains = '';

// YouTube TV
const yttvIDs = fs.readFileSync('./youtube-tv/ids.txt', { encoding: 'utf8' }).split('\n');
domains += yttvIDs
  .map(id =>
    Array.from({ length: 6 })
      .map((_item, index) => `r${index + 1}---sn-${id}.googlevideo.com`)
      .join('\n')
  )
  .join('\n');

fs.writeFileSync('../dist/blacklist.txt', domains);
